# taskManagement



## Installation
### Downloawd
Download the files above and place on your server.

### Environment Files
This package ships with no `.env` file in the root of the project.

You must create this file and set the variabls value of your enviroment.

### Composer
Laravel project dependencies are managed through the PHP Composer tool. The first step is to install the depencencies by navigating into your project in terminal and typing this command:

```bash
composer install
```

### NPM
In order to install the Javascript packages for frontend development, you will need the Node Package Manager.

If you only have NPM installed you have to run this command from the root of the project:

```bash
npm install
```
Generate webpack

```bash
npm run dev
```

### Create Database
You must create your database on your server and on your `.env` file update the following lines:

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=taskmanagementlaravel
DB_USERNAME=root
DB_PASSWORD=
```
Change these lines to reflect your new database settings.


### Artisan Commands
The first thing we are going to do is set the key that Laravel will use when doing encryption.

```bash
php artisan key:generate
```
You should see a green message stating your key was successfully generated. As well as you should see the `APP_KEY` variable in your `.env` file reflected.


It's time to see if your database credentials are correct.

We are going to run the built in migrations to create the database tables:

```bash
php artisan migrate
```
You should see a message for each table migrated, if you don't and see errors, than your credentials are most likely not correct.

## Login
After your project is installed and you can access it in a browser, click the login button on the right of the navigation bar.

## Register
After your project is installed and you can access it in a browser, click the register button on the right of the navigation bar.
