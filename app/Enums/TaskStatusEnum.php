<?php

namespace App\Enums;

use Illuminate\Support\Collection;

enum TaskStatusEnum:string
{
    case Pending = 'pending';
    case InProgress = 'inprogress';
    case Finished = 'finished';

    static function values(): Collection {
        $collection = new Collection(self::cases());

        return $collection->pluck('value');
    }
    static function getStatus($status): array
    {
        $statuses = [
            'pending' => [
                "title" => "Pending",
                "value" => "pending",
                "class" => "warning"
            ],
            'inprogress' => [
                "title" => "In progress",
                "value" => "inprogress",
                "class" => "success"
            ],
            'finished' => [
                "title" => "Finished",
                "value" => "finished",
                "class" => "info"
            ],
        ];
        return $statuses[$status];
    }
}
