<?php

namespace App\Http\Controllers;

use App\Enums\TaskStatusEnum;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use App\Services\TaskService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class TaskController extends Controller
{
    private TaskService $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('tasks.index');
    }

    /**
     * return datatable paginated data
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function search(Request $request): mixed
    {
        return DataTables::of(Auth::user()->tasks()->latest()->get())
            ->addColumn('title', function($row) {
                return $row->title;
            })->addColumn('description', function($row) {
                return $row->description;
            })->addColumn('estimation', function($row) {
                return $row->estimation;
            })->addColumn('start_date', function($row) {
                return $row->start_date;
            })->addColumn('end_date', function($row) {
                if ($row->end_date != null ){
                    $diff = $row->start_date->diffInHours($row->end_date);
                    if ($diff > $row->estimation){
                        return $row->end_date.'<br><span class="badge bg-danger">Delayed with '.$diff - $row->estimation.' H /estimated</span>';
                    }else{
                        return $row->end_date.'<br><span class="badge bg-success">Advanced with '.$row->estimation- $diff.' H /estimated</span>';
                    }
                }
                return $row->end_date;
            })->addColumn('status', function($row) {
                return TaskStatusEnum::getStatus($row->status);;
            })->addColumn('created_at', function($row) {
                return $row->created_at->diffForHumans();
            })
            ->addColumn('id', function($row) {
                return $row->id;
            })
            ->rawColumns(['title','description','estimation','start_date', 'end_date', 'status', 'created_at', 'id'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTaskRequest $request
     * @return RedirectResponse
     */
    public function store(StoreTaskRequest $request): RedirectResponse
    {
        try {
            $this->taskService->store($request->validated());
        }catch(Exception $e){
            return back()->with('error','Failed to create Task');
        }
        return back()->with('success','Task was successfully created');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTaskRequest $request
     * @param Task $task
     * @return RedirectResponse
     */
    public function update(UpdateTaskRequest $request, Task $task): RedirectResponse
    {
        try {
            $this->taskService->update($request->validated(),$task);
        }catch(Exception $e){
            return back()->with('error','Failed to update Task');
        }
        return back()->with('success','Task was successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $task_id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Task $task): JsonResponse
    {
        if($task->user_id != Auth::id()){
            return Response()->json(['success' => false],422);
        }
        if($this->taskService->deleteById($task_id)){
            return Response()->json(['success' => true]);
        }
        return Response()->json(['success' => false]);
    }
}
