<?php

namespace App\Services;

use App\Enums\TaskStatusEnum;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;

/**
 * Class TaskService
 * for the eloquent task management
 */
class TaskService extends BaseService
{
    /**
     * Inject task model
     * @param Task $task
     */
    public function __construct(Task $task)
    {
        $this->model = $task;
    }

    /**
     * Eloquent Task creation
     * @param array $data
     * @return Task|bool
     */
    public function store( array $data) : Task|bool
    {
        try {
            $task = $this->model::create([
                'title' => $data['title'] ?? null,
                'description' => $data['description'] ?? null,
                'estimation' => $data['estimation'] ?? null,
                'user_id' => $data['user_id'] ??Auth::id() ?? ,
            ]);
        }catch (\Exception $e){
            return false;
        }
        return $task;
    }

    /**
     * Eloquent Task update
     * @param array $data
     * @param Task $task
     * @return Task|bool
     */
    public function update( array $data,Task $task) : Task|bool
    {

        if($data['status'] == TaskStatusEnum::InProgress->value && $task->status == TaskStatusEnum::Pending->value) {
            $data['start_date'] = now();
        }
        if($data['status'] == TaskStatusEnum::Finished->value && $task->status == TaskStatusEnum::InProgress->value) {
            $data['end_date'] = now();
        }
        try {
            $task->update([
                'title' => $data['title'] ?? $task->title,
                'description' => $data['description'] ?? $task->description,
                'estimation' => $data['estimation'] ?? $task->estimation,
                'start_date' => $data['start_date'] ?? $task->start_date,
                'end_date' => $data['end_date'] ?? $task->end_date,
                'status' => $data['status'] ?? $task->status,
            ]);
        }catch (\Exception $e){
            return false;
        }
        return $task;
    }

    /**
     *
     * Update only status of task
     * @param $status
     * @param Task $task
     * @return Task|bool
     */
    public function updateStatus($status,Task $task) : Task|bool
    {
        $data = [
            'status' => $data['status'] ?? $task->status,
        ];
        try {
            if($status == TaskStatusEnum::InProgress)$data['start_date'] = today();
            if($status == TaskStatusEnum::Finished)$data['end_date'] = today();
            $task->update($data);
        }catch (\Exception $e){
            return false;
        }
        return $task;
    }

}
