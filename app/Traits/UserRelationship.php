<?php

namespace App\Traits;

use App\Models\Task;

trait UserRelationship
{
    public function tasks(){
        return $this->hasMany(Task::class,'user_id');
    }
}
