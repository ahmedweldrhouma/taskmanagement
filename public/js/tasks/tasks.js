/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/tasks.js":
/*!*******************************!*\
  !*** ./resources/js/tasks.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("__webpack_require__(/*! ./tasks/index */ \"./resources/js/tasks/index.js\");\n__webpack_require__(/*! ./tasks/remove */ \"./resources/js/tasks/remove.js\");\n__webpack_require__(/*! ./tasks/edit */ \"./resources/js/tasks/edit.js\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvdGFza3MuanMiLCJtYXBwaW5ncyI6IkFBQUFBLG1CQUFPLENBQUMsb0RBQWUsQ0FBQztBQUN4QkEsbUJBQU8sQ0FBQyxzREFBZ0IsQ0FBQztBQUN6QkEsbUJBQU8sQ0FBQyxrREFBYyxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Rhc2tzLmpzPzk0MWUiXSwic291cmNlc0NvbnRlbnQiOlsicmVxdWlyZSgnLi90YXNrcy9pbmRleCcpO1xucmVxdWlyZSgnLi90YXNrcy9yZW1vdmUnKTtcbnJlcXVpcmUoJy4vdGFza3MvZWRpdCcpO1xuIl0sIm5hbWVzIjpbInJlcXVpcmUiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/js/tasks.js\n");

/***/ }),

/***/ "./resources/js/tasks/edit.js":
/*!************************************!*\
  !*** ./resources/js/tasks/edit.js ***!
  \************************************/
/***/ (() => {

eval("$(document).on('click', '.edit', function (e) {\n  var tr = $(this).closest('tr');\n  var data = window.table.row(tr).data();\n  console.log(data);\n  var modal = document.getElementById(\"edit-task-modal\");\n  var myModal = new bootstrap.Modal(modal, {});\n  modal.querySelector('[name=\"title\"]').value = data.title;\n  modal.querySelector('[name=\"description\"]').value = data.description;\n  modal.querySelector('[name=\"estimation\"]').value = data.estimation;\n  modal.querySelector('[name=\"status\"]').value = data.status.value;\n  modal.querySelector('form').action = route('tasks.update', data.id);\n  myModal.show();\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJvbiIsImUiLCJ0ciIsImNsb3Nlc3QiLCJkYXRhIiwid2luZG93IiwidGFibGUiLCJyb3ciLCJjb25zb2xlIiwibG9nIiwibW9kYWwiLCJnZXRFbGVtZW50QnlJZCIsIm15TW9kYWwiLCJib290c3RyYXAiLCJNb2RhbCIsInF1ZXJ5U2VsZWN0b3IiLCJ2YWx1ZSIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJlc3RpbWF0aW9uIiwic3RhdHVzIiwiYWN0aW9uIiwicm91dGUiLCJpZCIsInNob3ciXSwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Rhc2tzL2VkaXQuanM/ZDFkOSJdLCJzb3VyY2VzQ29udGVudCI6WyIkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmVkaXQnLCBmdW5jdGlvbiAoZSkge1xuICAgIGxldCB0ciA9ICQodGhpcykuY2xvc2VzdCgndHInKTtcbiAgICBsZXQgZGF0YSA9IHdpbmRvdy50YWJsZS5yb3codHIpLmRhdGEoKTtcbiAgICBjb25zb2xlLmxvZyhkYXRhKTtcbiAgICB2YXIgbW9kYWwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImVkaXQtdGFzay1tb2RhbFwiKTtcbiAgICB2YXIgbXlNb2RhbCA9IG5ldyBib290c3RyYXAuTW9kYWwobW9kYWwsIHt9KTtcbiAgICBtb2RhbC5xdWVyeVNlbGVjdG9yKCdbbmFtZT1cInRpdGxlXCJdJykudmFsdWUgPWRhdGEudGl0bGU7XG4gICAgbW9kYWwucXVlcnlTZWxlY3RvcignW25hbWU9XCJkZXNjcmlwdGlvblwiXScpLnZhbHVlID1kYXRhLmRlc2NyaXB0aW9uO1xuICAgIG1vZGFsLnF1ZXJ5U2VsZWN0b3IoJ1tuYW1lPVwiZXN0aW1hdGlvblwiXScpLnZhbHVlID1kYXRhLmVzdGltYXRpb247XG4gICAgbW9kYWwucXVlcnlTZWxlY3RvcignW25hbWU9XCJzdGF0dXNcIl0nKS52YWx1ZSA9ZGF0YS5zdGF0dXMudmFsdWU7XG4gICAgbW9kYWwucXVlcnlTZWxlY3RvcignZm9ybScpLmFjdGlvbj1yb3V0ZSgndGFza3MudXBkYXRlJyxkYXRhLmlkKTtcbiAgICBteU1vZGFsLnNob3coKTtcbn0pO1xuIl0sIm1hcHBpbmdzIjoiQUFBQUEsQ0FBQyxDQUFDQyxRQUFRLENBQUMsQ0FBQ0MsRUFBRSxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsVUFBVUMsQ0FBQyxFQUFFO0VBQzFDLElBQUlDLEVBQUUsR0FBR0osQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDSyxPQUFPLENBQUMsSUFBSSxDQUFDO0VBQzlCLElBQUlDLElBQUksR0FBR0MsTUFBTSxDQUFDQyxLQUFLLENBQUNDLEdBQUcsQ0FBQ0wsRUFBRSxDQUFDLENBQUNFLElBQUksQ0FBQyxDQUFDO0VBQ3RDSSxPQUFPLENBQUNDLEdBQUcsQ0FBQ0wsSUFBSSxDQUFDO0VBQ2pCLElBQUlNLEtBQUssR0FBR1gsUUFBUSxDQUFDWSxjQUFjLENBQUMsaUJBQWlCLENBQUM7RUFDdEQsSUFBSUMsT0FBTyxHQUFHLElBQUlDLFNBQVMsQ0FBQ0MsS0FBSyxDQUFDSixLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7RUFDNUNBLEtBQUssQ0FBQ0ssYUFBYSxDQUFDLGdCQUFnQixDQUFDLENBQUNDLEtBQUssR0FBRVosSUFBSSxDQUFDYSxLQUFLO0VBQ3ZEUCxLQUFLLENBQUNLLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDQyxLQUFLLEdBQUVaLElBQUksQ0FBQ2MsV0FBVztFQUNuRVIsS0FBSyxDQUFDSyxhQUFhLENBQUMscUJBQXFCLENBQUMsQ0FBQ0MsS0FBSyxHQUFFWixJQUFJLENBQUNlLFVBQVU7RUFDakVULEtBQUssQ0FBQ0ssYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUNDLEtBQUssR0FBRVosSUFBSSxDQUFDZ0IsTUFBTSxDQUFDSixLQUFLO0VBQy9ETixLQUFLLENBQUNLLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQ00sTUFBTSxHQUFDQyxLQUFLLENBQUMsY0FBYyxFQUFDbEIsSUFBSSxDQUFDbUIsRUFBRSxDQUFDO0VBQ2hFWCxPQUFPLENBQUNZLElBQUksQ0FBQyxDQUFDO0FBQ2xCLENBQUMsQ0FBQyIsImZpbGUiOiIuL3Jlc291cmNlcy9qcy90YXNrcy9lZGl0LmpzIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/js/tasks/edit.js\n");

/***/ }),

/***/ "./resources/js/tasks/index.js":
/*!*************************************!*\
  !*** ./resources/js/tasks/index.js ***!
  \*************************************/
/***/ (() => {

eval("window.table = $('#tasks-table').DataTable({\n  ajax: {\n    url: route('tasks.search'),\n    method: \"POST\",\n    data: {\n      _token: $('meta[name=\"csrf-token\"]').attr('content')\n    }\n  },\n  \"columns\": [{\n    \"data\": \"title\",\n    \"name\": \"title\"\n  }, {\n    \"data\": \"description\",\n    \"name\": \"description\"\n  }, {\n    \"data\": \"estimation\",\n    \"name\": \"estimation\"\n  }, {\n    \"data\": \"start_date\",\n    \"name\": \"start_date\"\n  }, {\n    \"data\": \"end_date\",\n    \"name\": \"end_date\"\n  }, {\n    \"data\": \"status\",\n    \"name\": \"status\",\n    render: function render(data) {\n      return '<span class=\"badge bg-' + data[\"class\"] + '\">' + data.title + '</span>';\n    }\n  }, {\n    \"data\": \"created_at\",\n    \"name\": \"created_at\"\n  }, {\n    \"data\": \"id\",\n    \"name\": \"id\",\n    render: function render(id, display, row) {\n      if (row.status.value !== \"finished\") {\n        return '<a href=\"#\" class=\"edit btn btn-primary btn-sm mx-2 edit\" data-id=\"' + id + '\">Edit</a><button type=\"button\" class=\"btn btn-secondary btn-sm remove\" data-id=\"' + id + '\">Remove</button>';\n      }\n      return \"\";\n    }\n  }],\n  processing: true,\n  serverSide: true\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvdGFza3MvaW5kZXguanMiLCJuYW1lcyI6WyJ3aW5kb3ciLCJ0YWJsZSIsIiQiLCJEYXRhVGFibGUiLCJhamF4IiwidXJsIiwicm91dGUiLCJtZXRob2QiLCJkYXRhIiwiX3Rva2VuIiwiYXR0ciIsInJlbmRlciIsInRpdGxlIiwiaWQiLCJkaXNwbGF5Iiwicm93Iiwic3RhdHVzIiwidmFsdWUiLCJwcm9jZXNzaW5nIiwic2VydmVyU2lkZSJdLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2pzL3Rhc2tzL2luZGV4LmpzPzA3YjEiXSwic291cmNlc0NvbnRlbnQiOlsid2luZG93LnRhYmxlID0gJCgnI3Rhc2tzLXRhYmxlJykuRGF0YVRhYmxlKHtcbiAgICBhamF4OiB7XG4gICAgICAgIHVybDogcm91dGUoJ3Rhc2tzLnNlYXJjaCcpLFxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICBfdG9rZW46ICQoJ21ldGFbbmFtZT1cImNzcmYtdG9rZW5cIl0nKS5hdHRyKCdjb250ZW50JylcbiAgICAgICAgfVxuICAgIH0sXG4gICAgXCJjb2x1bW5zXCI6IFtcbiAgICAgICAge1wiZGF0YVwiOiBcInRpdGxlXCIsIFwibmFtZVwiOiBcInRpdGxlXCJ9LFxuICAgICAgICB7XCJkYXRhXCI6IFwiZGVzY3JpcHRpb25cIiwgXCJuYW1lXCI6IFwiZGVzY3JpcHRpb25cIn0sXG4gICAgICAgIHtcImRhdGFcIjogXCJlc3RpbWF0aW9uXCIsIFwibmFtZVwiOiBcImVzdGltYXRpb25cIn0sXG4gICAgICAgIHtcImRhdGFcIjogXCJzdGFydF9kYXRlXCIsIFwibmFtZVwiOiBcInN0YXJ0X2RhdGVcIn0sXG4gICAgICAgIHtcImRhdGFcIjogXCJlbmRfZGF0ZVwiLCBcIm5hbWVcIjogXCJlbmRfZGF0ZVwifSxcbiAgICAgICAge1xuICAgICAgICAgICAgXCJkYXRhXCI6IFwic3RhdHVzXCIsIFwibmFtZVwiOiBcInN0YXR1c1wiLCByZW5kZXI6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICc8c3BhbiBjbGFzcz1cImJhZGdlIGJnLScgKyBkYXRhLmNsYXNzICsgJ1wiPicgKyBkYXRhLnRpdGxlICsgJzwvc3Bhbj4nXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHtcImRhdGFcIjogXCJjcmVhdGVkX2F0XCIsIFwibmFtZVwiOiBcImNyZWF0ZWRfYXRcIn0sXG4gICAgICAgIHtcbiAgICAgICAgICAgIFwiZGF0YVwiOiBcImlkXCIsIFwibmFtZVwiOiBcImlkXCIsIHJlbmRlcjogZnVuY3Rpb24gKGlkLCBkaXNwbGF5LCByb3cpIHtcbiAgICAgICAgICAgICAgICBpZiAocm93LnN0YXR1cy52YWx1ZSAhPT0gXCJmaW5pc2hlZFwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnPGEgaHJlZj1cIiNcIiBjbGFzcz1cImVkaXQgYnRuIGJ0bi1wcmltYXJ5IGJ0bi1zbSBteC0yIGVkaXRcIiBkYXRhLWlkPVwiJyArIGlkICsgJ1wiPkVkaXQ8L2E+PGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJidG4gYnRuLXNlY29uZGFyeSBidG4tc20gcmVtb3ZlXCIgZGF0YS1pZD1cIicgKyBpZCArICdcIj5SZW1vdmU8L2J1dHRvbj4nO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gXCJcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICBdLFxuICAgIHByb2Nlc3Npbmc6IHRydWUsXG4gICAgc2VydmVyU2lkZTogdHJ1ZSxcbn0pO1xuIl0sIm1hcHBpbmdzIjoiQUFBQUEsTUFBTSxDQUFDQyxLQUFLLEdBQUdDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQ0MsU0FBUyxDQUFDO0VBQ3ZDQyxJQUFJLEVBQUU7SUFDRkMsR0FBRyxFQUFFQyxLQUFLLENBQUMsY0FBYyxDQUFDO0lBQzFCQyxNQUFNLEVBQUUsTUFBTTtJQUNkQyxJQUFJLEVBQUU7TUFDRkMsTUFBTSxFQUFFUCxDQUFDLENBQUMseUJBQXlCLENBQUMsQ0FBQ1EsSUFBSSxDQUFDLFNBQVM7SUFDdkQ7RUFDSixDQUFDO0VBQ0QsU0FBUyxFQUFFLENBQ1A7SUFBQyxNQUFNLEVBQUUsT0FBTztJQUFFLE1BQU0sRUFBRTtFQUFPLENBQUMsRUFDbEM7SUFBQyxNQUFNLEVBQUUsYUFBYTtJQUFFLE1BQU0sRUFBRTtFQUFhLENBQUMsRUFDOUM7SUFBQyxNQUFNLEVBQUUsWUFBWTtJQUFFLE1BQU0sRUFBRTtFQUFZLENBQUMsRUFDNUM7SUFBQyxNQUFNLEVBQUUsWUFBWTtJQUFFLE1BQU0sRUFBRTtFQUFZLENBQUMsRUFDNUM7SUFBQyxNQUFNLEVBQUUsVUFBVTtJQUFFLE1BQU0sRUFBRTtFQUFVLENBQUMsRUFDeEM7SUFDSSxNQUFNLEVBQUUsUUFBUTtJQUFFLE1BQU0sRUFBRSxRQUFRO0lBQUVDLE1BQU0sRUFBRSxTQUFBQSxPQUFVSCxJQUFJLEVBQUU7TUFDeEQsT0FBTyx3QkFBd0IsR0FBR0EsSUFBSSxTQUFNLEdBQUcsSUFBSSxHQUFHQSxJQUFJLENBQUNJLEtBQUssR0FBRyxTQUFTO0lBQ2hGO0VBQ0osQ0FBQyxFQUNEO0lBQUMsTUFBTSxFQUFFLFlBQVk7SUFBRSxNQUFNLEVBQUU7RUFBWSxDQUFDLEVBQzVDO0lBQ0ksTUFBTSxFQUFFLElBQUk7SUFBRSxNQUFNLEVBQUUsSUFBSTtJQUFFRCxNQUFNLEVBQUUsU0FBQUEsT0FBVUUsRUFBRSxFQUFFQyxPQUFPLEVBQUVDLEdBQUcsRUFBRTtNQUM1RCxJQUFJQSxHQUFHLENBQUNDLE1BQU0sQ0FBQ0MsS0FBSyxLQUFLLFVBQVUsRUFBRTtRQUNqQyxPQUFPLHFFQUFxRSxHQUFHSixFQUFFLEdBQUcsbUZBQW1GLEdBQUdBLEVBQUUsR0FBRyxtQkFBbUI7TUFDdE07TUFDQSxPQUFPLEVBQUU7SUFDYjtFQUNKLENBQUMsQ0FDSjtFQUNESyxVQUFVLEVBQUUsSUFBSTtFQUNoQkMsVUFBVSxFQUFFO0FBQ2hCLENBQUMsQ0FBQyJ9\n//# sourceURL=webpack-internal:///./resources/js/tasks/index.js\n");

/***/ }),

/***/ "./resources/js/tasks/remove.js":
/*!**************************************!*\
  !*** ./resources/js/tasks/remove.js ***!
  \**************************************/
/***/ (() => {

eval("$(document).on('click', '.remove', function (e) {\n  var id = $(this).data('id');\n  var tr = $(this).closest('tr');\n  Swal.fire({\n    title: 'Are you sure?',\n    text: \"You won't be able to revert this!\",\n    icon: 'warning',\n    showCancelButton: true,\n    confirmButtonColor: '#3085d6',\n    cancelButtonColor: '#d33',\n    confirmButtonText: 'Yes, delete it!'\n  }).then(function (result) {\n    if (result.isConfirmed) {\n      $.ajax({\n        url: route('tasks.delete', id),\n        method: \"DELETE\",\n        headers: {\n          'X-CSRF-TOKEN': $('meta[name=\"csrf-token\"]').attr('content')\n        },\n        success: function success(data) {\n          Swal.fire('Deleted!', 'Your task has been deleted.', 'success');\n          window.table.row(tr).remove().draw();\n        },\n        error: function error() {\n          Swal.fire('Error!', 'Error while deleting task.', 'error');\n        }\n      });\n    }\n  });\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJvbiIsImUiLCJpZCIsImRhdGEiLCJ0ciIsImNsb3Nlc3QiLCJTd2FsIiwiZmlyZSIsInRpdGxlIiwidGV4dCIsImljb24iLCJzaG93Q2FuY2VsQnV0dG9uIiwiY29uZmlybUJ1dHRvbkNvbG9yIiwiY2FuY2VsQnV0dG9uQ29sb3IiLCJjb25maXJtQnV0dG9uVGV4dCIsInRoZW4iLCJyZXN1bHQiLCJpc0NvbmZpcm1lZCIsImFqYXgiLCJ1cmwiLCJyb3V0ZSIsIm1ldGhvZCIsImhlYWRlcnMiLCJhdHRyIiwic3VjY2VzcyIsIndpbmRvdyIsInRhYmxlIiwicm93IiwicmVtb3ZlIiwiZHJhdyIsImVycm9yIl0sInNvdXJjZXMiOlsid2VicGFjazovLy8uL3Jlc291cmNlcy9qcy90YXNrcy9yZW1vdmUuanM/MTZjMyJdLCJzb3VyY2VzQ29udGVudCI6WyIkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLnJlbW92ZScsIGZ1bmN0aW9uIChlKSB7XG4gICAgbGV0IGlkID0gJCh0aGlzKS5kYXRhKCdpZCcpO1xuICAgIGxldCB0ciA9ICQodGhpcykuY2xvc2VzdCgndHInKTtcbiAgICBTd2FsLmZpcmUoe1xuICAgICAgICB0aXRsZTogJ0FyZSB5b3Ugc3VyZT8nLFxuICAgICAgICB0ZXh0OiBcIllvdSB3b24ndCBiZSBhYmxlIHRvIHJldmVydCB0aGlzIVwiLFxuICAgICAgICBpY29uOiAnd2FybmluZycsXG4gICAgICAgIHNob3dDYW5jZWxCdXR0b246IHRydWUsXG4gICAgICAgIGNvbmZpcm1CdXR0b25Db2xvcjogJyMzMDg1ZDYnLFxuICAgICAgICBjYW5jZWxCdXR0b25Db2xvcjogJyNkMzMnLFxuICAgICAgICBjb25maXJtQnV0dG9uVGV4dDogJ1llcywgZGVsZXRlIGl0ISdcbiAgICB9KS50aGVuKChyZXN1bHQpID0+IHtcbiAgICAgICAgaWYgKHJlc3VsdC5pc0NvbmZpcm1lZCkge1xuICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICB1cmw6IHJvdXRlKCd0YXNrcy5kZWxldGUnLGlkKSxcbiAgICAgICAgICAgICAgICBtZXRob2Q6XCJERUxFVEVcIixcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7ICdYLUNTUkYtVE9LRU4nOiAkKCdtZXRhW25hbWU9XCJjc3JmLXRva2VuXCJdJykuYXR0cignY29udGVudCcpIH0sXG4gICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgU3dhbC5maXJlKFxuICAgICAgICAgICAgICAgICAgICAgICAgJ0RlbGV0ZWQhJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdZb3VyIHRhc2sgaGFzIGJlZW4gZGVsZXRlZC4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3N1Y2Nlc3MnXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LnRhYmxlXG4gICAgICAgICAgICAgICAgICAgICAgICAucm93KHRyKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlbW92ZSgpXG4gICAgICAgICAgICAgICAgICAgICAgICAuZHJhdygpO1xuICAgICAgICAgICAgICAgIH0sZXJyb3I6ZnVuY3Rpb24gKCl7XG4gICAgICAgICAgICAgICAgICAgIFN3YWwuZmlyZShcbiAgICAgICAgICAgICAgICAgICAgICAgICdFcnJvciEnLFxuICAgICAgICAgICAgICAgICAgICAgICAgJ0Vycm9yIHdoaWxlIGRlbGV0aW5nIHRhc2suJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdlcnJvcidcbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICB9KVxufSlcbiJdLCJtYXBwaW5ncyI6IkFBQUFBLENBQUMsQ0FBQ0MsUUFBUSxDQUFDLENBQUNDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVVDLENBQUMsRUFBRTtFQUM1QyxJQUFJQyxFQUFFLEdBQUdKLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQ0ssSUFBSSxDQUFDLElBQUksQ0FBQztFQUMzQixJQUFJQyxFQUFFLEdBQUdOLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQ08sT0FBTyxDQUFDLElBQUksQ0FBQztFQUM5QkMsSUFBSSxDQUFDQyxJQUFJLENBQUM7SUFDTkMsS0FBSyxFQUFFLGVBQWU7SUFDdEJDLElBQUksRUFBRSxtQ0FBbUM7SUFDekNDLElBQUksRUFBRSxTQUFTO0lBQ2ZDLGdCQUFnQixFQUFFLElBQUk7SUFDdEJDLGtCQUFrQixFQUFFLFNBQVM7SUFDN0JDLGlCQUFpQixFQUFFLE1BQU07SUFDekJDLGlCQUFpQixFQUFFO0VBQ3ZCLENBQUMsQ0FBQyxDQUFDQyxJQUFJLENBQUMsVUFBQ0MsTUFBTSxFQUFLO0lBQ2hCLElBQUlBLE1BQU0sQ0FBQ0MsV0FBVyxFQUFFO01BQ3BCbkIsQ0FBQyxDQUFDb0IsSUFBSSxDQUFDO1FBQ0hDLEdBQUcsRUFBRUMsS0FBSyxDQUFDLGNBQWMsRUFBQ2xCLEVBQUUsQ0FBQztRQUM3Qm1CLE1BQU0sRUFBQyxRQUFRO1FBQ2ZDLE9BQU8sRUFBRTtVQUFFLGNBQWMsRUFBRXhCLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDeUIsSUFBSSxDQUFDLFNBQVM7UUFBRSxDQUFDO1FBQ3pFQyxPQUFPLEVBQUUsU0FBQUEsUUFBVXJCLElBQUksRUFBRTtVQUNyQkcsSUFBSSxDQUFDQyxJQUFJLENBQ0wsVUFBVSxFQUNWLDZCQUE2QixFQUM3QixTQUNKLENBQUM7VUFDRGtCLE1BQU0sQ0FBQ0MsS0FBSyxDQUNQQyxHQUFHLENBQUN2QixFQUFFLENBQUMsQ0FDUHdCLE1BQU0sQ0FBQyxDQUFDLENBQ1JDLElBQUksQ0FBQyxDQUFDO1FBQ2YsQ0FBQztRQUFDQyxLQUFLLEVBQUMsU0FBQUEsTUFBQSxFQUFXO1VBQ2Z4QixJQUFJLENBQUNDLElBQUksQ0FDTCxRQUFRLEVBQ1IsNEJBQTRCLEVBQzVCLE9BQ0osQ0FBQztRQUNMO01BQ0osQ0FBQyxDQUFDO0lBQ047RUFDSixDQUFDLENBQUM7QUFDTixDQUFDLENBQUMiLCJmaWxlIjoiLi9yZXNvdXJjZXMvanMvdGFza3MvcmVtb3ZlLmpzIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/js/tasks/remove.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./resources/js/tasks.js");
/******/ 	
/******/ })()
;