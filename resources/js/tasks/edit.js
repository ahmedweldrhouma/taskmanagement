$(document).on('click', '.edit', function (e) {
    let tr = $(this).closest('tr');
    let data = window.table.row(tr).data();
    console.log(data);
    var modal = document.getElementById("edit-task-modal");
    var myModal = new bootstrap.Modal(modal, {});
    modal.querySelector('[name="title"]').value =data.title;
    modal.querySelector('[name="description"]').value =data.description;
    modal.querySelector('[name="estimation"]').value =data.estimation;
    modal.querySelector('[name="status"]').value =data.status.value;
    modal.querySelector('form').action=route('tasks.update',data.id);
    myModal.show();
});
