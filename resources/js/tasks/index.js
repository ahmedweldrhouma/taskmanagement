window.table = $('#tasks-table').DataTable({
    ajax: {
        url: route('tasks.search'),
        method: "POST",
        data: {
            _token: $('meta[name="csrf-token"]').attr('content')
        }
    },
    "columns": [
        {"data": "title", "name": "title"},
        {"data": "description", "name": "description"},
        {"data": "estimation", "name": "estimation"},
        {"data": "start_date", "name": "start_date"},
        {"data": "end_date", "name": "end_date"},
        {
            "data": "status", "name": "status", render: function (data) {
                return '<span class="badge bg-' + data.class + '">' + data.title + '</span>'
            }
        },
        {"data": "created_at", "name": "created_at"},
        {
            "data": "id", "name": "id", render: function (id, display, row) {
                if (row.status.value !== "finished") {
                    return '<a href="#" class="edit btn btn-primary btn-sm mx-2 edit" data-id="' + id + '">Edit</a><button type="button" class="btn btn-secondary btn-sm remove" data-id="' + id + '">Remove</button>';
                }
                return "";
            }
        },
    ],
    processing: true,
    serverSide: true,
});
