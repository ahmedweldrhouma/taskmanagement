$(document).on('click', '.remove', function (e) {
    let id = $(this).data('id');
    let tr = $(this).closest('tr');
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: route('tasks.delete',id),
                method:"DELETE",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                success: function (data) {
                    Swal.fire(
                        'Deleted!',
                        'Your task has been deleted.',
                        'success'
                    )
                    window.table
                        .row(tr)
                        .remove()
                        .draw();
                },error:function (){
                    Swal.fire(
                        'Error!',
                        'Error while deleting task.',
                        'error'
                    )
                }
            })
        }
    })
})
