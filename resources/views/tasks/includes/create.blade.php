<div class="modal fade" id="create-task-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content" action="{{ route('tasks.store') }}" method="post">
            <div class="modal-header">
                <h5 class="modal-title">Create Task</h5>
                <button type="button" class="close btn btn-transparent" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @csrf
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" required id="title"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="estimation">Estimation (in hours)</label>
                        <input type="number" class="form-control" name="estimation" required id="estimation"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" required id="description"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
