<div class="modal fade" id="edit-task-modal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content" action="" method="post" id="update-task-form">
            <div class="modal-header">
                <h5 class="modal-title">Update Task</h5>
                <button type="button" class="close btn btn-transparent" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @csrf
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" required />
                    </div>
                    <div class="form-group col-md-6">
                        <label>Estimation (in hours)</label>
                        <input type="number" class="form-control" name="estimation" required />
                    </div>
                    <div class="form-group col-md-6">
                        <label>Status</label>
                        <select class="form-control" name="status" required >
                            @foreach(\App\Enums\TaskStatusEnum::cases() as $status)
                                <option value="{{ $status->value }}">{{ __($status->value) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
</div>
