@extends('layouts.app')
@push('style')
    <link rel='stylesheet' href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" media='all'/>
@endpush
@section('content')
    <div class="row">
        <div class="card col-md-10 mx-auto">
            <div class="card-header d-flex justify-content-between">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-gray-800">Tasks List</span>
                </h3>
                <div class="card-toolbar">
                    <a href="#" class="btn btn-primary justify-content-end" data-bs-toggle="modal"
                       data-bs-target="#create-task-modal">
                        Create Task
                    </a>
                </div>
            </div>
            <div class="card-body w-100">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="w-100 table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer"
                           id="tasks-table">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Estimation (Hours)</th>
                            <th>Start_date</th>
                            <th>End date</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('tasks.includes.create')
    @include('tasks.includes.edit')
@endsection
@push('after-script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.4.1/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('js/tasks/tasks.js') }}"></script>
@endpush
