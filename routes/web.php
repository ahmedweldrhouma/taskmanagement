<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Auth\LoginController::class,'showLoginForm']);

Auth::routes();
Route::middleware('auth')->group(function (){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/tasks',[App\Http\Controllers\TaskController::class,'index'])->name('tasks.index');
    Route::post('/tasks',[App\Http\Controllers\TaskController::class,'store'])->name('tasks.store');
    Route::post('/tasks/{task}',[App\Http\Controllers\TaskController::class,'update'])->name('tasks.update');
    Route::delete('/tasks/{task}',[App\Http\Controllers\TaskController::class,'destroy'])->name('tasks.delete');
    Route::post('/tasks-search',[App\Http\Controllers\TaskController::class,'search'])->name('tasks.search');
});
