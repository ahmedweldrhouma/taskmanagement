<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_tasks_screen_can_be_rendered()
    {
        $response = $this->get('/tasks');

        $response->assertStatus(200);
    }

    public function test_tasks_data_can_be_rendered()
    {
        $response = $this->post('/tasks/search',['__token'=>csrf_token()]);

        $response->assertStatus(200);
    }


    public function test_user_can_create_task()
    {
        $task = Task::factory()->create();

        $response = $this->post('/tasks', [
            'title' => $task->title,
            'description' => $task->description,
            'estimation' => $task->estimation,
        ]);
        $response->assertStatus(200);
    }



    public function test_user_can_update_task()
    {
        $task = Task::factory()->create();

        $response = $this->post('/tasks/'.$task->id, [
            'title' => 'titre changé',
            'description' => $task->description,
            'estimation' => $task->estimation,
        ]);
        $response->assertStatus(200);
    }


    public function test_user_can_delete_task()
    {
        $task = Task::factory()->create();
        $response = $this->delete('/tasks/'.$task->id);
        $response->assertStatus(200);
    }

}
