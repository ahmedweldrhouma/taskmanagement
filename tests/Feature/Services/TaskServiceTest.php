<?php

namespace Tests\Feature\Services;

use App\Models\Task;
use App\Services\TaskService;
use Tests\TestCase;

class TaskServiceTest extends TestCase
{

    public function test__construct(TaskService $service)
    {
        $this->service = $service;
    }

    public function it_can_create_a_task()
    {
        $task = Task::factory()->create();

        $taskCreated = $this->service->store([
            'title' => $task->title,
            'description' => $task->description,
            'estimation' => $task->estimation,
            'user_id' => $task->user_id,
        ]);
        $this->assertInstanceOf(Task::class, $task);
        $this->assertEquals($taskCreated->title, $task->title);
        $this->assertEquals($taskCreated->description, $task->description);
        $this->assertEquals($taskCreated->estimation, $task->estimation);
        $this->assertEquals($taskCreated->user_id, $task->user_id);
    }

    public function it_can_update_a_task()
    {
        $task = Task::factory()->create();

        $taskUpdated = $this->service->update([
            'title' => "other title ",
            'description' => $task->description,
            'estimation' => $task->estimation,
            'user_id' => $task->user_id,
        ], $task);
        $this->assertInstanceOf(Task::class, $task);
        $this->assertEquals($taskUpdated->title, "other title");
        $this->assertEquals($taskUpdated->description, $task->description);
        $this->assertEquals($taskUpdated->estimation, $task->estimation);
        $this->assertEquals($taskUpdated->user_id, $task->user_id);
    }


}
